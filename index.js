



const getCube = 2*8;
console.log(`The cube of 2 is ${getCube}`);


const Address = ["5 mangga rd", "Signal Village", "Taguig City"]


// console.log(Address[0]);
// console.log(Address[1]);
// console.log(Address[2]);

//Array Desctructing Re-Assign
const[streets,village,city] = Address;


console.log(`${streets} ${village} ${city}`)

const animal = {
	name: "Lolong",
	type: "salwater crocodile",
	w: 1075,
	h: "20 ft 3 in"
};

const{name,type,w,h} = animal;

console.log(`${name} was a ${type}. He weight at ${w} kgs with a measurment of ${h} `);


const array = [1,2,3,4,5];
array.forEach((number) =>console.log(number));


class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const newdogs = new Dog("New Dog",4,"Wolf");
console.log(newdogs);